import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-add',
  templateUrl: 'add.html'
})
export class AddPage {

  zoek:string;
  key: string = "AllFood";
  picture:any;
  calorieen:number;
  message:string;
  Naam:string;
   Food:any ={
    picture : "",
    name: "",
    calorieen:""
  };
  AllFood =[];

  constructor(public navCtrl: NavController,private camera: Camera,private storage: Storage) {

    this.loadData();

  }

  loadData(){
    this.storage.get(this.key).then((val) => {
      if(val!=null)
      {
        this.AllFood=val;
      }
    });
  }
  SaveData() {

    if (this.Naam!=null ||this.calorieen!=null) {

      console.log(this.picture,this.Naam,this.calorieen);
      console.log(this.AllFood);
      this.Food.picture=this.picture;
      this.Food.name=this.Naam;
      this.Food.calorieen = this.calorieen;

      this.AllFood.push(this.Food);

      this.storage.set(this.key,this.AllFood);
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
    } else {
      this.message="Gelieve de velden in te vullen"
    }
  }

  DeleteData()
  {
    this.AllFood.splice(  this.AllFood.indexOf(this.zoek), 1 );
    this.storage.set(this.key,this.AllFood);
  }

//take Photo
takePhoto(sourceType:number) {
  const options: CameraOptions = {
    quality: 50,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,
    sourceType:sourceType,
  }

  this.camera.getPicture(options).then((imageData) => {
    this.picture = 'data:image/jpeg;base64,' + imageData;
  }, (err) => {
    // Handle error
  });
}
}
