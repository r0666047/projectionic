import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { HomePage } from "../home/home";


@Component({
  selector: "page-settings",
  templateUrl: "settings.html"
})
export class SettingsPage {
  key: string = "caolorieperday";
  input: number;
  choice: string;
  calorieperday: number;
  standatcaldifren:number = 500;
  message:string;

  constructor(public navCtrl: NavController, private storage: Storage) {}

  SaveData() {
    if (Number.isNaN((this.input)*1) || this.choice==null) {
      this.message="gelieve all velden in te vullen";
    } else {
      switch (this.choice) {
        case "loos":
        this.calorieperday = (this.input)*1 - this.standatcaldifren;
          break;
        case "equal":
        this.calorieperday = (this.input)*1;
          break;
        case "gain":
        this.calorieperday = (this.input)*1 + this.standatcaldifren;
          break;
        default:
        console.log("somthing went wrong whit the choice",this.choice)
          break;
      }
      this.storage.set(this.key, this.calorieperday);
      this.navCtrl.push(HomePage);
    }

  }
}
