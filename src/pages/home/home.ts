import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { Chart } from "chart.js";
import { ViewChild } from "@angular/core";
import { Storage } from "@ionic/storage";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  @ViewChild("doughnutCanvas") doughnutCanvas;
  CalorieLeft: number ;
  CalorieConsumed: number;
  doughnutChart: any;
  key2: string = "consumed";
  key: string = "caolorieperday";
  calorieperday: number ;
  message: string;
  constructor(public navCtrl: NavController, private storage: Storage) {
    this.GetVaribles();
  }
  resetCounter()
  {
    this.storage.set(this.key2,0);
    this.CalorieConsumed=0;
    this.displayChart();
  }

  GetVaribles()
  {
    this.storage.get(this.key2).then(val => {
      if (Number.isNaN(val)) {
        this.CalorieConsumed = 0;
      } else {
        this.CalorieConsumed = val;
      }
    });

    this.storage.get(this.key).then(val => {
      this.calorieperday = val;
      this.CalorieLeft = this.calorieperday - this.CalorieConsumed;
      if (Number.isNaN(this.calorieperday) || Number.isNaN(this.CalorieLeft)) {
        console.log("er is nog geen data");
        this.message = "Ga eerst naar instellingen en vul daar de gegevens in";
      } else {
        this.displayChart();
        this.message = "Je hebt " + this.CalorieLeft + " calorieën over";
      }
    });
  }

  displayChart() {
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [this.CalorieLeft, this.CalorieConsumed],
            backgroundColor: ["rgba(0, 255, 0, 1)", "rgba(255, 0, 0, 1)"]
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        title: {
          display: false,
          fontStyle: "bold",
          fontSize: 18
        }
      }
    });
  }
}
