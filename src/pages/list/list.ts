import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { Storage } from "@ionic/storage";

@Component({
  selector: "page-list",
  templateUrl: "list.html"
})
export class ListPage {
  key: string = "AllFood";
  selectedItem: any;
  AllFood = [];
key2: string = "consumed";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage
  ) {

    this.storage.get(this.key).then(val => {
      if (val != null) {
        this.AllFood = val;
      }
    });
  }

  itemTapped(item) {
    this.storage.get(this.key2).then(val => {
      this.storage.set(this.key2,val+item*1);
    });
  }
}
